package org.meveo.api.dto.response.billing;

import org.meveo.api.dto.BaseEntityDto;
import org.meveo.api.dto.billing.WalletInstanceDto;
import org.meveo.api.dto.response.BaseResponse;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "GetWalletInstanceResponseDto")
@XmlAccessorType(XmlAccessType.FIELD)
public class GetWalletInstanceResponseDto extends BaseResponse {

    private WalletInstanceDto walletInstanceDto;

    public WalletInstanceDto getWalletInstanceDto() {
        return walletInstanceDto;
    }

    public void setWalletInstanceDto(WalletInstanceDto walletInstanceDto) {
        this.walletInstanceDto = walletInstanceDto;
    }
}
