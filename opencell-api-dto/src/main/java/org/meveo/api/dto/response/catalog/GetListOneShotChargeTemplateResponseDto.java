package org.meveo.api.dto.response.catalog;

import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

import org.meveo.api.dto.catalog.OneShotChargeTemplateDto;
import org.meveo.api.dto.response.BaseResponse;

@XmlRootElement(name = "GetListOneShotChargeTemplateResponse")
@XmlAccessorType(XmlAccessType.FIELD)
public class GetListOneShotChargeTemplateResponseDto extends BaseResponse {
	
	private static final long serialVersionUID = 7799704813768522245L;
	
	private List<OneShotChargeTemplateDto> oneShotChargeTemplates;

	public List<OneShotChargeTemplateDto> getOneShotChargeTemplates() {
		return oneShotChargeTemplates;
	}

	public void setOneShotChargeTemplates(List<OneShotChargeTemplateDto> oneShotChargeTemplates) {
		this.oneShotChargeTemplates = oneShotChargeTemplates;
	}
	
}
