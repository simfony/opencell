package org.meveo.api.dto.response.catalog;

import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlRootElement;

import org.meveo.api.dto.catalog.TriggeredEdrTemplateDto;
import org.meveo.api.dto.response.SearchResponse;

@XmlRootElement(name = "GetListOfferTemplateResponse")
@XmlAccessorType(XmlAccessType.FIELD)
public class GetListTriggeredEdrResponseDto extends SearchResponse {

	private static final long serialVersionUID = -6696608963995671789L;
	
	@XmlElementWrapper(name = "triggeredEdrTemplates")
	@XmlElement(name = "triggeredEdrTemplate")
	private List<TriggeredEdrTemplateDto> triggeredEdrTemplates;

	public List<TriggeredEdrTemplateDto> getTriggeredEdrTemplates() {
		return triggeredEdrTemplates;
	}

	public void setTriggeredEdrTemplates(List<TriggeredEdrTemplateDto> triggeredEdrTemplates) {
		this.triggeredEdrTemplates = triggeredEdrTemplates;
	}
	
	

}
