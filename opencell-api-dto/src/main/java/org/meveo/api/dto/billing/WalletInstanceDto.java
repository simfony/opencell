package org.meveo.api.dto.billing;

import org.meveo.model.billing.WalletInstance;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import java.math.BigDecimal;

@XmlAccessorType(XmlAccessType.FIELD)
public class WalletInstanceDto {
    private String userAccountCode;
    private String code;
    private String description;
    private BigDecimal lowBalanceLevel;
    private BigDecimal rejectLevel;
    private BigDecimal balance;

    public WalletInstanceDto() {
    }

    public WalletInstanceDto(WalletInstance walletInstance, BigDecimal balance) {
        this.code = walletInstance.getCode();
        this.description = walletInstance.getDescription();
        this.lowBalanceLevel = walletInstance.getLowBalanceLevel();
        this.rejectLevel = walletInstance.getRejectLevel();
        this.userAccountCode = walletInstance.getUserAccount().getCode();
        this.balance = balance;
    }

    public String getUserAccountCode() {
        return userAccountCode;
    }

    public void setUserAccountCode(String userAccountCode) {
        this.userAccountCode = userAccountCode;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public BigDecimal getLowBalanceLevel() {
        return lowBalanceLevel;
    }

    public void setLowBalanceLevel(BigDecimal lowBalanceLevel) {
        this.lowBalanceLevel = lowBalanceLevel;
    }

    public BigDecimal getRejectLevel() {
        return rejectLevel;
    }

    public void setRejectLevel(BigDecimal rejectLevel) {
        this.rejectLevel = rejectLevel;
    }

    public BigDecimal getBalance() {
        return balance;
    }

    public void setBalance(BigDecimal balance) {
        this.balance = balance;
    }

    @Override
    public String toString() {
        return "WalletInstanceDto{" +
                "userAccountCode='" + userAccountCode + '\'' +
                ", code='" + code + '\'' +
                ", description='" + description + '\'' +
                ", lowBalanceLevel=" + lowBalanceLevel +
                ", rejectLevel=" + rejectLevel +
                ", balance=" + balance +
                '}';
    }
}
