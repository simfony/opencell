package org.meveo.api.dto.response;

import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

import org.meveo.api.dto.ScriptInstanceDto;

@XmlRootElement(name = "GetListScriptInstanceResponse")
@XmlAccessorType(XmlAccessType.FIELD)
public class GetListScriptInstanceResponseDto extends BaseResponse {

	private static final long serialVersionUID = 5776143896458756591L;

	private List<ScriptInstanceDto> scriptInstances;

	public List<ScriptInstanceDto> getScriptInstances() {
		return scriptInstances;
	}

	public void setScriptInstances(List<ScriptInstanceDto> scriptInstances) {
		this.scriptInstances = scriptInstances;
	}

}
