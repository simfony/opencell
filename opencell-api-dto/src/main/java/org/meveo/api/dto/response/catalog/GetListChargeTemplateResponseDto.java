package org.meveo.api.dto.response.catalog;

import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

import org.meveo.api.dto.catalog.ChargeTemplateDto;
import org.meveo.api.dto.response.BaseResponse;

@XmlRootElement(name = "GetListChargeTemplateResponse")
@XmlAccessorType(XmlAccessType.FIELD)
public class GetListChargeTemplateResponseDto extends BaseResponse {

	private static final long serialVersionUID = -8207749298447687359L;
	
	private List<ChargeTemplateDto> chargeTemplates;

	public List<ChargeTemplateDto> getChargeTemplates() {
		return chargeTemplates;
	}

	public void setChargeTemplates(List<ChargeTemplateDto> chargeTemplates) {
		this.chargeTemplates = chargeTemplates;
	}

}
