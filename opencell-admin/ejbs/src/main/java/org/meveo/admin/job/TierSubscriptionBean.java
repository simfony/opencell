package org.meveo.admin.job;

import org.apache.commons.lang3.tuple.ImmutablePair;
import org.meveo.admin.async.SubListCreator;
import org.meveo.admin.async.TierSubscriptionAsync;
import org.meveo.model.jobs.JobExecutionResultImpl;
import org.meveo.model.jobs.JobInstance;
import org.meveo.security.MeveoUser;
import org.meveo.service.billing.impl.SubscriptionService;
import org.slf4j.Logger;

import javax.ejb.Stateless;
import javax.inject.Inject;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;

@Stateless
public class TierSubscriptionBean extends BaseJobBean {

    @Inject
    private Logger log;

    @Inject
    private SubscriptionService subscriptionService;

    @Inject
    private TierSubscriptionAsync tierSubscriptionAsync;

    public void execute(JobExecutionResultImpl result, JobInstance jobInstance) {
        Long nbRuns = (Long) this.getParamOrCFValue(jobInstance, "nbRuns", -1L);
        if (nbRuns == -1) {
            nbRuns = (long) Runtime.getRuntime().availableProcessors();
        }

        try {
            String billingAccountCode = (String) this.getParamOrCFValue(jobInstance, "billingAccountCode", null);
            String offerCode = (String) this.getParamOrCFValue(jobInstance, "offerCode", null);

            List<ImmutablePair> pairs = subscriptionService.getDistinctOfferAndBillingAccount(billingAccountCode, offerCode);
            result.setNbItemsToProcess(pairs.size());


            Long waitingMillis = (Long) this.getParamOrCFValue(jobInstance, "waitingMillis", 0L);

            List<Future<String>> futures = new ArrayList<Future<String>>();
            SubListCreator subListCreator = new SubListCreator(pairs, nbRuns.intValue());

            MeveoUser lastCurrentUser = currentUser.unProxy();
            while (subListCreator.isHasNext()) {
                futures.add(tierSubscriptionAsync.launchAndForget((List<ImmutablePair>) subListCreator.getNextWorkSet(), result, lastCurrentUser));
                if (subListCreator.isHasNext()) {
                    try {
                        Thread.sleep(waitingMillis.longValue());
                    } catch (InterruptedException e) {
                        log.error("", e);
                    }
                }
            }
            // Wait for all async methods to finish
            for (Future<String> future : futures) {
                try {
                    future.get();

                } catch (InterruptedException e) {
                    // It was cancelled from outside - no interest

                } catch (ExecutionException e) {
                    Throwable cause = e.getCause();
                    result.registerError(cause.getMessage());
                    result.addReport(cause.getMessage());
                    log.error("Failed to execute async method", cause);
                }
            }
        } catch (Exception e) {
            log.error("Failed to run usage rating job", e);
            result.registerError(e.getMessage());
            result.addReport(e.getMessage());
        }
    }
}
