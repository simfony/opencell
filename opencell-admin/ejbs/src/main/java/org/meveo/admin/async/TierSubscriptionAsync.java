package org.meveo.admin.async;

import org.apache.commons.lang3.tuple.ImmutablePair;
import org.meveo.admin.job.UnitTierSubscriptionJobBean;
import org.meveo.model.jobs.JobExecutionResultImpl;
import org.meveo.security.MeveoUser;
import org.meveo.security.keycloak.CurrentUserProvider;
import org.meveo.service.job.JobExecutionService;

import javax.ejb.AsyncResult;
import javax.ejb.Asynchronous;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.inject.Inject;
import java.util.List;
import java.util.concurrent.Future;

@Stateless
public class TierSubscriptionAsync {

    @Inject
    private JobExecutionService jobExecutionService;

    @Inject
    private CurrentUserProvider currentUserProvider;

    @Inject
    private UnitTierSubscriptionJobBean unitTierSubscriptionJobBean;

    @Asynchronous
    @TransactionAttribute(TransactionAttributeType.NEVER)
    public Future<String> launchAndForget(List<ImmutablePair> nextWorkSet, JobExecutionResultImpl result, MeveoUser lastCurrentUser) {
        currentUserProvider.reestablishAuthentication(lastCurrentUser);

        int i = 0;
        for (ImmutablePair pair : nextWorkSet) {
            i++;
            if (i % JobExecutionService.CHECK_IS_JOB_RUNNING_EVERY_NR == 0 && !jobExecutionService.isJobRunningOnThis(result.getJobInstance().getId())) {
                break;
            }
            unitTierSubscriptionJobBean.execute(result, pair);
        }
        return new AsyncResult<String>("OK");
    }
}
