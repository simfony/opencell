package org.meveo.admin.job;

import org.meveo.cache.WalletCacheContainerProvider;
import org.meveo.model.billing.WalletInstance;
import org.meveo.model.jobs.JobExecutionResultImpl;
import org.meveo.service.billing.impl.WalletService;
import org.slf4j.Logger;

import javax.ejb.Stateless;
import javax.inject.Inject;
import java.time.LocalDateTime;
import java.util.Date;
import java.util.List;

@Stateless
public class PrepaidAndSubscriptionStatusCheckBean {

    @Inject
    private Logger log;

    @Inject
    private WalletService walletService;

    @Inject
    private WalletCacheContainerProvider walletCacheContainerProvider;

    public void execute(JobExecutionResultImpl result) {
        log.debug("PopulateWalletExpirationDateJobBean executed at : {}", LocalDateTime.now());

        result.setStartDate(new Date());

        List<WalletInstance> walletsWithExpiredCredits = walletService.getWalletsWithExpiredCredits(new Date());
        result.addNbItemsToProcess(walletsWithExpiredCredits.size());

        walletsWithExpiredCredits.forEach(wallet -> {
            try {
                walletCacheContainerProvider.updateBalanceToZeroForAGivenWallet(wallet);
                result.addNbItemsCorrectlyProcessed(1);
            } catch (Exception e) {
                result.addNbItemsProcessedWithError(1);
                log.error("Error encountered when setting balance to zero for wallet with id [" + wallet.getId() + "]", e);
            }
        });

        result.setEndDate(new Date());
    }

}
