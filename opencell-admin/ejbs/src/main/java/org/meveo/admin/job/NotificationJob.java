package org.meveo.admin.job;

import org.meveo.admin.exception.BusinessException;
import org.meveo.model.crm.CustomFieldTemplate;
import org.meveo.model.crm.custom.CustomFieldTypeEnum;
import org.meveo.model.jobs.JobCategoryEnum;
import org.meveo.model.jobs.JobExecutionResultImpl;
import org.meveo.model.jobs.JobInstance;
import org.meveo.model.jobs.MeveoJobCategoryEnum;
import org.meveo.service.job.Job;

import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.inject.Inject;
import java.util.HashMap;
import java.util.Map;


/**
 * The Class NotificationJob apply recurring charge for next billingCycle.
 * @author Abdellatif BARI
 * @lastModifiedVersion 7.0
 */
@Stateless
public class NotificationJob extends Job {

    /** The recurring rating job bean. */
    @Inject
    private NotificationJobBean notificationJobBean;


    @Override
    @TransactionAttribute(TransactionAttributeType.NEVER)
    protected void execute(JobExecutionResultImpl result, JobInstance jobInstance) throws BusinessException {
        notificationJobBean.execute(result, jobInstance);
    }


    @Override
    public JobCategoryEnum getJobCategory() {
        return MeveoJobCategoryEnum.UTILS;
    }


    @Override
    public Map<String, CustomFieldTemplate> getCustomFields() {
        Map<String, CustomFieldTemplate> result = new HashMap<>();

        CustomFieldTemplate customFieldNbRuns = new CustomFieldTemplate();
        customFieldNbRuns.setCode("nbRuns");
        customFieldNbRuns.setAppliesTo("JobInstance_NotificationJob");
        customFieldNbRuns.setActive(true);
        customFieldNbRuns.setDescription(resourceMessages.getString("jobExecution.nbRuns"));
        customFieldNbRuns.setFieldType(CustomFieldTypeEnum.LONG);
        customFieldNbRuns.setValueRequired(false);
        customFieldNbRuns.setDefaultValue("-1");
        result.put("nbRuns", customFieldNbRuns);

        CustomFieldTemplate customFieldNbWaiting = new CustomFieldTemplate();
        customFieldNbWaiting.setCode("waitingMillis");
        customFieldNbWaiting.setAppliesTo("JobInstance_NotificationJob");
        customFieldNbWaiting.setActive(true);
        customFieldNbWaiting.setDescription(resourceMessages.getString("jobExecution.waitingMillis"));
        customFieldNbWaiting.setFieldType(CustomFieldTypeEnum.LONG);
        customFieldNbWaiting.setValueRequired(false);
        customFieldNbWaiting.setDefaultValue("0");
        result.put("waitingMillis", customFieldNbWaiting);

        CustomFieldTemplate httpWebhookNotification = new CustomFieldTemplate();
        httpWebhookNotification.setCode("notification");
        httpWebhookNotification.setAppliesTo("JobInstance_NotificationJob");
        httpWebhookNotification.setActive(true);
        httpWebhookNotification.setDescription(resourceMessages.getString("jobExecution.notification"));
        httpWebhookNotification.setFieldType(CustomFieldTypeEnum.ENTITY);
        httpWebhookNotification.setEntityClazz("org.meveo.model.notification.Notification");
        httpWebhookNotification.setValueRequired(true);
        result.put("notification", httpWebhookNotification);

        CustomFieldTemplate retries = new CustomFieldTemplate();
        retries.setCode("numberOfRetry");
        retries.setAppliesTo("JobInstance_NotificationJob");
        retries.setActive(true);
        retries.setDescription(resourceMessages.getString("jobExecution.retry"));
        retries.setFieldType(CustomFieldTypeEnum.LONG);
        retries.setValueRequired(false);
        retries.setDefaultValue("0");
        result.put("numberOfRetry", retries);

        CustomFieldTemplate retryInterval = new CustomFieldTemplate();
        retryInterval.setCode("retryInterval");
        retryInterval.setAppliesTo("JobInstance_NotificationJob");
        retryInterval.setActive(true);
        retryInterval.setDescription(resourceMessages.getString("jobExecution.retryInterval"));
        retryInterval.setFieldType(CustomFieldTypeEnum.LONG);
        retryInterval.setValueRequired(false);
        retryInterval.setDefaultValue("5");
        result.put("retryInterval", retryInterval);

        return result;
    }
}