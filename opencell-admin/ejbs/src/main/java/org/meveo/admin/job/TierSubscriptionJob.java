package org.meveo.admin.job;

import org.meveo.admin.exception.BusinessException;
import org.meveo.model.crm.CustomFieldTemplate;
import org.meveo.model.crm.custom.CustomFieldTypeEnum;
import org.meveo.model.jobs.JobCategoryEnum;
import org.meveo.model.jobs.JobExecutionResultImpl;
import org.meveo.model.jobs.JobInstance;
import org.meveo.model.jobs.MeveoJobCategoryEnum;
import org.meveo.service.job.Job;

import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.inject.Inject;
import java.util.HashMap;
import java.util.Map;

@Stateless
public class TierSubscriptionJob extends Job {

    private String APPLIES_TO = "JobInstance_TierSubscriptionJob";

    @Inject
    private TierSubscriptionBean tierSubscriptionBean;

    @Override
    @TransactionAttribute(TransactionAttributeType.NEVER)
    protected void execute(JobExecutionResultImpl result, JobInstance jobInstance) throws BusinessException {
        tierSubscriptionBean.execute(result, jobInstance);
    }

    @Override
    public JobCategoryEnum getJobCategory() {
        return MeveoJobCategoryEnum.INVOICING;
    }

    @Override
    public Map<String, CustomFieldTemplate> getCustomFields() {
        Map<String, CustomFieldTemplate> result = new HashMap<>();

        CustomFieldTemplate customFieldNbRuns = new CustomFieldTemplate();
        customFieldNbRuns.setCode("nbRuns");
        customFieldNbRuns.setAppliesTo(APPLIES_TO);
        customFieldNbRuns.setActive(true);
        customFieldNbRuns.setDescription(resourceMessages.getString("jobExecution.nbRuns"));
        customFieldNbRuns.setFieldType(CustomFieldTypeEnum.LONG);
        customFieldNbRuns.setValueRequired(false);
        customFieldNbRuns.setDefaultValue("-1");
        customFieldNbRuns.setGuiPosition("tab:Custom fields:0;fieldGroup:Configuration:0;field:0");
        result.put("nbRuns", customFieldNbRuns);

        CustomFieldTemplate waitingMillis = new CustomFieldTemplate();
        waitingMillis.setCode("waitingMillis");
        waitingMillis.setAppliesTo(APPLIES_TO);
        waitingMillis.setActive(true);
        waitingMillis.setDescription(resourceMessages.getString("jobExecution.waitingMillis"));
        waitingMillis.setFieldType(CustomFieldTypeEnum.LONG);
        waitingMillis.setValueRequired(false);
        waitingMillis.setDefaultValue("0");
        waitingMillis.setGuiPosition("tab:Custom fields:0;fieldGroup:Configuration:0;field:1");
        result.put("waitingMillis", waitingMillis);

        CustomFieldTemplate cfCriteriaParam1 = new CustomFieldTemplate();
        cfCriteriaParam1.setCode("billingAccountCode");
        cfCriteriaParam1.setAppliesTo(APPLIES_TO);
        cfCriteriaParam1.setActive(true);
        cfCriteriaParam1.setDescription(resourceMessages.getString("subscriptionTierJob.billingAccountCode"));
        cfCriteriaParam1.setFieldType(CustomFieldTypeEnum.STRING);
        cfCriteriaParam1.setValueRequired(false);
        cfCriteriaParam1.setMaxValue(50L);
        cfCriteriaParam1.setGuiPosition("tab:Custom fields:0;fieldGroup:Criteria:1;field:0");
        result.put("billingAccountCode", cfCriteriaParam1);

        CustomFieldTemplate cfCriteriaParam2 = new CustomFieldTemplate();
        cfCriteriaParam2.setCode("offerCode");
        cfCriteriaParam2.setAppliesTo(APPLIES_TO);
        cfCriteriaParam2.setActive(true);
        cfCriteriaParam2.setDescription(resourceMessages.getString("subscriptionTierJob.offerCode"));
        cfCriteriaParam2.setFieldType(CustomFieldTypeEnum.STRING);
        cfCriteriaParam2.setValueRequired(false);
        cfCriteriaParam2.setMaxValue(50L);
        cfCriteriaParam2.setGuiPosition("tab:Custom fields:0;fieldGroup:Criteria:1;field:1");
        result.put("offerCode", cfCriteriaParam2);

        return result;
    }
}
