package org.meveo.admin.job;

import org.apache.commons.lang3.tuple.Triple;
import org.meveo.admin.exception.BusinessException;
import org.meveo.model.jobs.JobCategoryEnum;
import org.meveo.model.jobs.JobExecutionResultImpl;
import org.meveo.model.jobs.JobInstance;
import org.meveo.model.jobs.MeveoJobCategoryEnum;
import org.meveo.service.billing.impl.ReRatingBasedOnPricePlanService;
import org.meveo.service.job.Job;

import javax.ejb.Stateless;
import javax.inject.Inject;
import java.util.Date;
import java.util.List;

@Stateless
public class ReRatingBasedOnPricePlanJob extends Job {

    @Inject
    private ReRatingBasedOnPricePlanService reRatingBasedOnPricePlanService;

    @Override
    protected void execute(JobExecutionResultImpl result, JobInstance jobInstance) throws BusinessException {
        result.setStartDate(new Date());

        reRatingBasedOnPricePlanService.updateStatusToReRateForApplicableTransactions();

        Triple<Integer, Integer, List<String>> resultFromOperations = reRatingBasedOnPricePlanService.reRateApplicableTransactions();

        result.setNbItemsToProcess(resultFromOperations.getLeft());
        result.setNbItemsCorrectlyProcessed(resultFromOperations.getMiddle());
        result.setNbItemsProcessedWithError(resultFromOperations.getRight().size());

        resultFromOperations.getRight().forEach(result::registerError);

        result.setEndDate(new Date());
    }

    @Override
    public JobCategoryEnum<?> getJobCategory() {
        return MeveoJobCategoryEnum.RATING;
    }
}
