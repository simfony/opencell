package org.meveo.admin.job;

import org.apache.commons.lang.NotImplementedException;
import org.meveo.admin.exception.BusinessException;
import org.meveo.model.crm.EntityReferenceWrapper;
import org.meveo.model.jobs.JobExecutionResultImpl;
import org.meveo.model.jobs.JobInstance;
import org.meveo.model.notification.Notification;
import org.meveo.model.notification.WebHook;
import org.meveo.security.CurrentUser;
import org.meveo.security.MeveoUser;
import org.meveo.service.notification.GenericNotificationService;
import org.meveo.service.notification.WebHookNotifier;
import org.slf4j.Logger;

import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.inject.Inject;
import java.util.HashMap;
import java.util.Map;

@Stateless
public class NotificationJobBean extends BaseJobBean {

    @Inject
    private Logger log;

    @Inject
    private GenericNotificationService notificationService;

    @Inject
    private WebHookNotifier webHookNotifier;

    @Inject
    @CurrentUser
    protected MeveoUser currentUser;

    @SuppressWarnings({"unchecked", "rawtypes"})
    @TransactionAttribute(TransactionAttributeType.NEVER)
    public void execute(JobExecutionResultImpl result, JobInstance jobInstance) {

        Long nbRuns = (Long) this.getParamOrCFValue(jobInstance, "nbRuns", -1L);
        if (nbRuns == -1) {
            nbRuns = (long) Runtime.getRuntime().availableProcessors();
        }
        Long waitingMillis = (Long) this.getParamOrCFValue(jobInstance, "waitingMillis", 0L);
        String notificationCode = ((EntityReferenceWrapper) this.getParamOrCFValue(jobInstance, "notification")).getCode();
        Long retryInterval = (Long) this.getParamOrCFValue(jobInstance, "retryInterval");
        Long numberOfRetry = (Long) this.getParamOrCFValue(jobInstance, "numberOfRetry");
        numberOfRetry = numberOfRetry + 1;

        try {

            if (notificationCode == null) {
                log.error("Failed to run notification job: missing notification code");
                result.registerError("KO");
                result.addReport("KO");
                return;
            }
            Notification notification = notificationService.findByCode(notificationCode);


            if (!(notification instanceof WebHook)) {
                throw new NotImplementedException(notification.getClassNameFilter() + "not supported yet");
            }

            Map<String, Object> context = new HashMap<>();
            context.put("currentUser", currentUser);
            long successAfter = 0L;
            boolean isSuccessful = false;
            for (long i = 0; i < numberOfRetry; i++) {
                try {
                    successAfter = i;
                    webHookNotifier.sendRequestJob((WebHook) notification, jobInstance, context, currentUser);
                    isSuccessful = true;
                    break;
                } catch (Exception e) {
                    Thread.sleep(retryInterval * 1000);
                }
            }

            if (!isSuccessful) {
                throw new RuntimeException("No success after " + successAfter + " retries");
            }

            result.addNbItemsCorrectlyProcessed(1);
            result.addReport("SUCCESS " + successAfter + " RETRY");
        } catch (Exception e) {
            log.error("Failed to run notification job", e);
            result.registerError(e.getMessage());
        }
    }

}
