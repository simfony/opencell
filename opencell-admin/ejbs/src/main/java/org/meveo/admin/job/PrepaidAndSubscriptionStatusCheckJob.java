package org.meveo.admin.job;

import org.meveo.admin.exception.BusinessException;
import org.meveo.admin.job.logging.JobLoggingInterceptor;
import org.meveo.interceptor.PerformanceInterceptor;
import org.meveo.model.jobs.*;
import org.meveo.service.job.Job;

import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.interceptor.Interceptors;
import java.time.LocalDateTime;

@Stateless
public class PrepaidAndSubscriptionStatusCheckJob extends Job {

    @Inject
    private PrepaidAndSubscriptionStatusCheckBean prepaidAndSubscriptionStatusCheckBean;

    @Override
    public JobCategoryEnum<?> getJobCategory() {
        return MeveoJobCategoryEnum.WALLET;
    }

    @Interceptors({ JobLoggingInterceptor.class, PerformanceInterceptor.class })
    @Override
    protected void execute(JobExecutionResultImpl result, JobInstance jobInstance) throws BusinessException {
        log.debug("PopulateWalletExpirationDateJob executed at : {}", LocalDateTime.now());

        prepaidAndSubscriptionStatusCheckBean.execute(result);
    }
}
