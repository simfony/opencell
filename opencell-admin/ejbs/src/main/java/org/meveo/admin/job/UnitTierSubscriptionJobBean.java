package org.meveo.admin.job;

import org.apache.commons.lang3.tuple.ImmutablePair;
import org.meveo.admin.job.logging.JobLoggingInterceptor;
import org.meveo.interceptor.PerformanceInterceptor;
import org.meveo.jpa.JpaAmpNewTx;
import org.meveo.model.billing.Subscription;
import org.meveo.model.catalog.OfferTemplate;
import org.meveo.model.jobs.JobExecutionResultImpl;
import org.meveo.service.billing.impl.SubscriptionService;
import org.meveo.service.catalog.impl.OfferTemplateService;
import org.slf4j.Logger;

import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.inject.Inject;
import javax.interceptor.Interceptors;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@Stateless
public class UnitTierSubscriptionJobBean {

    public static final String CUSTOM_FIELD_TIER_LEVELS = "TIER_LEVELS";
    public static final String CUSTOM_FIELD_TYPE = "TYPE";
    public static final String CUSTOM_FIELD_TIER = "Tier";
    public static final String TIER_TYPE_INCREMENTAL = "INCREMENTAL";
    public static final String TIER_TYPE_RETROACTIVE="RETROACTIVE";
    @Inject
    private Logger log;

    @Inject
    private SubscriptionService subscriptionService;

    @Inject
    private OfferTemplateService offerTemplateService;

    @JpaAmpNewTx
    @Interceptors({JobLoggingInterceptor.class, PerformanceInterceptor.class})
    @TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
    public void execute(JobExecutionResultImpl result, ImmutablePair<Long, Long> pair) {
        OfferTemplate offerTemplate = offerTemplateService.findById(pair.left);
        List<Subscription> subscriptions = subscriptionService.getByOfferAndBillingAccount(pair.left, pair.right);

        String tierType = (String) offerTemplate.getCfValue(CUSTOM_FIELD_TYPE);
        if (tierType == null) {
            result.registerError(String.format("No Tier type set for offer id: %s", pair.left));
            return;
        }
        String errorMessage;
        if (!tierType.equals(TIER_TYPE_INCREMENTAL) && !tierType.equals(TIER_TYPE_RETROACTIVE)) {
            errorMessage = String.format("Tier type is not set or has incorrect value on Offer Id: %s", offerTemplate.getId());
            log.warn(errorMessage);
            result.registerError(errorMessage);
            return;
        }
        Object rawTiers = offerTemplate.getCfValues().getValue(CUSTOM_FIELD_TIER_LEVELS);
        if (!(rawTiers instanceof Map)) {
            errorMessage = String.format("%s parameter is missing or has incorrect format for Offer Id: %s", CUSTOM_FIELD_TIER_LEVELS,
                    offerTemplate.getId());
            log.warn(errorMessage);
            result.registerError(errorMessage);
            return;
        }
        Map<String, String> tiers;
        try {
            tiers = (Map<String, String>) rawTiers;
        } catch (Exception e) {
            errorMessage = String.format("%s parameter contains values other that type 'Long' for Offer Id: %s", CUSTOM_FIELD_TIER_LEVELS,
                    offerTemplate.getId());
            log.warn(errorMessage);
            result.registerError(errorMessage);
            return;
        }
        List<Long> bounds = getBoundsForTiers(tiers);
        if (bounds == null) {
            errorMessage = String.format("%s parameter is incorrect for Offer Id: %s", CUSTOM_FIELD_TIER_LEVELS, offerTemplate.getId());
            log.warn(errorMessage);
            result.registerError(errorMessage);
            return;
        }
        applyTiersToSubscription(subscriptions, bounds, tierType, result);
    }

    private List<Long> getBoundsForTiers(Map<String, String> tiers) {
        Map<Long, Long> lTiers = new HashMap<>();
        for (Map.Entry<String, String> entry : tiers.entrySet()) {
            if (!entry.getKey().matches("\\d*") || !entry.getValue().matches("\\d*")) {
                return null;
            }
            lTiers.put(Long.parseLong(entry.getKey()), Long.parseLong(entry.getValue()));
        }
        List<Long> sortedTierNumbers = lTiers.keySet().stream().sorted().collect(Collectors.toList());
        // tiers should be consecutive numbers starting with 1
        // each tier should have bigger subscription number
        long index = 0;
        long subNumber = 0;
        List<Long> bounds = new ArrayList<>();
        for (Long number : sortedTierNumbers) {
            if ((index + 1) != number) {
                return null;
            }
            if (lTiers.get(number) <= subNumber) {
                return null;
            }
            subNumber = lTiers.get(number);
            index = number;
            bounds.add(subNumber);
        }
        return bounds;
    }

    private void applyTiersToSubscription(List<Subscription> subscriptions, List<Long> bounds, String tierType, JobExecutionResultImpl result) {
        if(tierType.equals(TIER_TYPE_INCREMENTAL)){
            int subCount = 0;
            for (Subscription subscription : subscriptions) {
                subCount++;
                long tier = 0;
                for (Long bound : bounds) {
                    tier++;
                    if ( subCount <= bound) {
                        break;
                    }
                }
                subscription.setCfValue(CUSTOM_FIELD_TIER, tier);
                log.debug("Applied tier {} for subscription {}", tier, subscription.getId());
                result.registerSucces();
            }
        } else if(tierType.equals(TIER_TYPE_RETROACTIVE)){
            long tier = 0;
            for (Long bound : bounds) {
                tier++;
                if (subscriptions.size()<=bound) {
                    break;
                }
            }
            for (Subscription subscription : subscriptions) {
                subscription.setCfValue(CUSTOM_FIELD_TIER, tier);
                log.debug("Applied tier {} for subscription {}", tier, subscription.getId());
                result.registerSucces();
            }
        }
    }

}
