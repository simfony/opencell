package org.meveo.admin.job;

import org.meveo.admin.job.logging.JobLoggingInterceptor;
import org.meveo.interceptor.PerformanceInterceptor;
import org.meveo.model.jobs.JobExecutionResultImpl;
import org.meveo.model.jobs.JobInstance;
import org.meveo.service.billing.impl.ReservationService;
import org.slf4j.Logger;

import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.inject.Inject;
import javax.interceptor.Interceptors;
import java.util.List;

@Stateless
public class WalletReservationJobBean extends BaseJobBean {

    @Inject
    private Logger log;

    @Inject
    private ReservationService reservationService;

    @Interceptors({JobLoggingInterceptor.class, PerformanceInterceptor.class})
    @TransactionAttribute(TransactionAttributeType.NEVER)
    public void execute(JobExecutionResultImpl result, JobInstance jobInstance) {
        List<Long> reservationsIds = reservationService.listExpiredReservations();
        result.setNbItemsToProcess(reservationsIds.size());
        int success = 0;
        int failed = 0;
        log.info("{} reservation needs to be cancelled for tenant {}", reservationsIds.size(), currentUser.getUserName());
        for (Long reservationId : reservationsIds) {
            try {
                log.info("{} preparing to be deleted for tenant {}", reservationId, currentUser.getUserName());
                reservationService.cancelPrepaidReservationInNewTransactionSetEDRStatus(reservationId);
                success++;
            } catch (Exception e) {
                failed++;
                result.registerError(e.getMessage());
                log.error(e.getMessage(), e);
            }
        }
        result.setNbItemsCorrectlyProcessed(success);
        result.setNbItemsProcessedWithError(failed);
    }

}