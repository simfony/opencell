package org.meveo.service.billing.impl;

import org.apache.commons.lang3.tuple.Triple;
import org.meveo.model.billing.UserAccount;
import org.meveo.model.billing.WalletOperation;
import org.meveo.service.base.PersistenceService;
import org.slf4j.Logger;

import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.inject.Inject;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Stateless
public class ReRatingBasedOnPricePlanService extends PersistenceService<WalletOperation> {

    @Inject
    private WalletOperationService walletOperationService;

    @Inject
    private UserAccountService userAccountService;

    @Inject
    private RatingService ratingService;

    @Inject
    protected Logger log;

    @TransactionAttribute(TransactionAttributeType.REQUIRED)
    public void updateStatusToReRateForApplicableTransactions() {
        List<UserAccount> userAccounts = userAccountService.getAllUserAccountsWithUpdatedOffers();

        // Next query is using an in(...) expression and will throw an exception if this list is empty
        if (userAccounts == null || userAccounts.isEmpty()) {
            return;
        }

        List<Long> walletIdsToReRate = userAccounts
                .stream()
                .map(userAccount -> userAccount.getWallet().getId())
                .collect(Collectors.toList());

        // Get all wallet operations that are not billed for the given wallets
        List<Long> notBilledWalletOperationsForWallets = walletOperationService.getNotBilledWalletOperationsForWallets(walletIdsToReRate);

        if (!notBilledWalletOperationsForWallets.isEmpty()) {
            // Set all wallet operations that are not billed to status 'TO_RERATE'.
            walletOperationService.updateToRerate(notBilledWalletOperationsForWallets);
        }
    }

    /**
     * Find all transactions belonging to a wallet and rerate these
     *
     * @return Triple.of(numberOfRecordsToProcess, numberOfSuccessfulRecords, List < String > errorMessages)
     */
    @TransactionAttribute(TransactionAttributeType.REQUIRED)
    public Triple<Integer, Integer, List<String>> reRateApplicableTransactions() {
        List<String> errorMessages = new ArrayList<>();
        int numberOfSuccessfulRecords = 0;

        // Get all wallet operations with status 'TO_RERATE'
        List<Long> walletOperationIds = walletOperationService.listToRerate();

        log.info("Rerate with useSamePricePlan={}, #operations={}", false, walletOperationIds.size());
        for (Long walletOperationId : walletOperationIds) {
            try {
                ratingService.reRate(walletOperationId, false);
                numberOfSuccessfulRecords++;
            } catch (Exception e) {
                log.error("Failed to rerate operation {} due to {}", walletOperationId, e.getMessage());
                errorMessages.add(e.getMessage());
            }
        }

        return Triple.of(walletOperationIds.size(), numberOfSuccessfulRecords, errorMessages);
    }
}
