package org.meveo.service.billing.impl;

import org.meveo.event.qualifier.Rejected;
import org.meveo.model.rating.EDR;

import javax.ejb.Asynchronous;
import javax.ejb.Stateless;
import javax.enterprise.event.Event;
import javax.inject.Inject;

@Stateless
public class FireNotificationAsync {

    @Inject
    @Rejected
    private Event<EDR> rejectedEDRProducer;

    @Asynchronous
    public void fireEdrRejectEvent(EDR edr){
        rejectedEDRProducer.fire(edr);
    }
}
