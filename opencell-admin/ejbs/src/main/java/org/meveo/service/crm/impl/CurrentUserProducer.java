/*
 * (C) Copyright 2015-2020 Opencell SAS (https://opencellsoft.com/) and contributors.
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General
 * Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option)
 * any later version.
 *
 * THERE IS NO WARRANTY FOR THE PROGRAM, TO THE EXTENT PERMITTED BY APPLICABLE LAW. EXCEPT WHEN
 * OTHERWISE STATED IN WRITING THE COPYRIGHT HOLDERS AND/OR OTHER PARTIES PROVIDE THE PROGRAM "AS
 * IS" WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE. THE ENTIRE RISK AS TO
 * THE QUALITY AND PERFORMANCE OF THE PROGRAM IS WITH YOU. SHOULD THE PROGRAM PROVE DEFECTIVE,
 * YOU ASSUME THE COST OF ALL NECESSARY SERVICING, REPAIR OR CORRECTION.
 *
 * For more information on the GNU Affero General Public License, please consult
 * <https://www.gnu.org/licenses/agpl-3.0.en.html>.
 */

package org.meveo.service.crm.impl;

import javax.ejb.Stateless;
import javax.enterprise.context.RequestScoped;
import javax.enterprise.inject.Instance;
import javax.enterprise.inject.Produces;
import javax.inject.Inject;
import javax.inject.Named;
import javax.persistence.EntityManager;
import javax.servlet.http.HttpServletRequest;

import org.meveo.commons.utils.StringUtils;
import org.meveo.jpa.EntityManagerProvider;
import org.meveo.security.CurrentUser;
import org.meveo.security.MeveoUser;
import org.meveo.security.keycloak.CurrentUserProvider;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Produce a currently authenticated user
 * 
 * @author Andrius Karpavicius
 */
@Stateless
public class CurrentUserProducer {

    private static final String X_TENANT_ID = "X-TenantID";
    private static final String AUTH = "Authorization";
    protected Logger log = LoggerFactory.getLogger(this.getClass());

    @Inject
    private CurrentUserProvider currentUserProvider;

    @Inject
    private Instance<HttpServletRequest> requestInstance;

    @Inject
    EntityManagerProvider entityManagerProvider;

    /**
     * Produce a current user
     *
     * @return MeveoUser Current user instance
     */
    @Produces
    @RequestScoped
    @Named("currentUser")
    @CurrentUser
    public MeveoUser getCurrentUser() {
        String providerCode = currentUserProvider.getCurrentUserProviderCode();
        HttpServletRequest request = getHttpServletRequest();

        if (request != null && request.getRequestURI().contains("/mediation") && request.getHeader(AUTH) == null) {
            String tenantIdHeader = request.getHeader(X_TENANT_ID);
            if (!StringUtils.isBlank(providerCode) && tenantIdHeader == null) {
                currentUserProvider.forceAuthentication(providerCode + ".superadmin", providerCode);
            }else {
                log.debug("Current providerCode is null/empty and current url contains /mediation. Trying to get provider code from {} http header...", X_TENANT_ID);

                if (StringUtils.isBlank(tenantIdHeader)) {
                    throw new IllegalStateException(String.format("%s header is blank!", X_TENANT_ID));
                }

                log.debug("Found {} header with value {}. Forcing auth this provider...", X_TENANT_ID, tenantIdHeader);
                currentUserProvider.forceAuthentication(tenantIdHeader + ".superadmin", tenantIdHeader);
                providerCode = currentUserProvider.getCurrentUserProviderCode();
            }
        }

        EntityManager em = entityManagerProvider.getEntityManager(providerCode);

        return currentUserProvider.getCurrentUser(providerCode, em);
    }

    private HttpServletRequest getHttpServletRequest() {
        HttpServletRequest request = null;
        try {
            request = requestInstance.get();
            request.getRequestURI();
        }catch (Exception e) {
            request = null;
        }

        return request;
    }

}