/*
 * (C) Copyright 2015-2020 Opencell SAS (https://opencellsoft.com/) and contributors.
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General
 * Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option)
 * any later version.
 *
 * THERE IS NO WARRANTY FOR THE PROGRAM, TO THE EXTENT PERMITTED BY APPLICABLE LAW. EXCEPT WHEN
 * OTHERWISE STATED IN WRITING THE COPYRIGHT HOLDERS AND/OR OTHER PARTIES PROVIDE THE PROGRAM "AS
 * IS" WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE. THE ENTIRE RISK AS TO
 * THE QUALITY AND PERFORMANCE OF THE PROGRAM IS WITH YOU. SHOULD THE PROGRAM PROVE DEFECTIVE,
 * YOU ASSUME THE COST OF ALL NECESSARY SERVICING, REPAIR OR CORRECTION.
 *
 * For more information on the GNU Affero General Public License, please consult
 * <https://www.gnu.org/licenses/agpl-3.0.en.html>.
 */

package org.meveo.api.billing;

import org.meveo.admin.exception.BusinessException;
import org.meveo.admin.exception.InsufficientBalanceException;
import org.meveo.admin.exception.RatingException;
import org.meveo.api.BaseApi;
import org.meveo.api.MeveoApiErrorCodeEnum;
import org.meveo.api.dto.billing.*;
import org.meveo.api.dto.response.billing.CdrReservationResponseDto;
import org.meveo.api.exception.MeveoApiException;
import org.meveo.commons.utils.StringUtils;
import org.meveo.model.billing.Reservation;
import org.meveo.model.billing.ReservationStatus;
import org.meveo.model.billing.WalletOperation;
import org.meveo.model.rating.CDR;
import org.meveo.model.rating.EDR;
import org.meveo.security.keycloak.CurrentUserProvider;
import org.meveo.service.billing.impl.EdrService;
import org.meveo.service.billing.impl.ReservationService;
import org.meveo.service.billing.impl.UsageRatingService;
import org.meveo.service.medina.impl.CDRParsingException;
import org.meveo.service.medina.impl.CDRParsingService;
import org.meveo.service.medina.impl.CSVCDRParser;
import org.meveo.service.notification.DefaultObserver;

import javax.ejb.Asynchronous;
import javax.ejb.Stateless;
import javax.inject.Inject;
import java.math.BigDecimal;
import java.time.OffsetDateTime;
import java.time.ZoneOffset;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * API for CDR processing and mediation handling in general
 *
 * @lastModifiedVersion willBeSetLater
 *
 * @author Andrius Karpavicius
 *
 */
@Stateless
public class MediationApi extends BaseApi {

    @Inject
    private CDRParsingService cdrParsingService;

    @Inject
    private EdrService edrService;

    @Inject
    private UsageRatingService usageRatingService;

    @Inject
    private ReservationService reservationService;

    @Inject
    private DefaultObserver defaultObserver;

    @Inject
    private CurrentUserProvider currentUserProvider;

    /**
     * Register EDRS
     *
     * @param postData String of CDRs. This CDR is parsed and created as EDR. CDR is same format use in mediation job
     * @throws MeveoApiException Meveo api exception
     * @throws BusinessException business exception.
     */
    public void registerCdrList(CdrListDto postData) throws MeveoApiException, BusinessException {
        List<String> cdrLines = postData.getCdr();
        if (cdrLines == null || cdrLines.size() == 0) {
            missingParameters.add("cdr");
        }

        handleMissingParameters();

        CSVCDRParser cdrParser = cdrParsingService.getCDRParser(currentUser.getUserName(), postData.getIpAddress());

        try {
            for (String line : cdrLines) {
                CDR cdr = cdrParser.parseCDR(line);
                cdrParsingService.createEdrs(cdr);
            }
        } catch (CDRParsingException e) {
            log.error("Error parsing cdr={}", e);
            throw new MeveoApiException(e.getMessage());
        }
    }

    /**
     * Register and rate EDRS
     *
     * @throws MeveoApiException Meveo api exception
     * @throws BusinessException business exception.
     */
    public ChargeCDRResponseDto chargeCdr(ChargeCDRDto chargeCDRDto) throws MeveoApiException, BusinessException {
        String cdrLine = chargeCDRDto.getCdr();
        if (StringUtils.isBlank(cdrLine)) {
            missingParameters.add("cdr");
        }

        if(chargeCDRDto.getIp() != null) {
            handleMissingParameters();
        }

        CSVCDRParser cdrParser = cdrParsingService.getCDRParser(currentUser.getUserName(), chargeCDRDto.getIp() != null ? chargeCDRDto.getIp(): null);

        try {
            CDR cdr = cdrParser.parseCDR(cdrLine);
            List<EDR> edrs = cdrParsingService.getEDRList(cdr);
            List<WalletOperation> walletOperations = new ArrayList<>();
            for (EDR edr : edrs) {
                log.debug("edr={}", edr);
                edr.setSubscription(edr.getSubscription());
                if (!chargeCDRDto.isVirtual()) {
                    edrService.create(edr);
                }
                List<WalletOperation> wo = rateUsage(edr, chargeCDRDto);
                if (wo != null) {
                    walletOperations.addAll(wo);
                }
            }

            return createChargeCDRResultDto(walletOperations, chargeCDRDto.isReturnWalletOperations());

        } catch (CDRParsingException e) {
            log.error("Error parsing cdr={}", e.getRejectionCause());
            throw new MeveoApiException(MeveoApiErrorCodeEnum.INVALID_ACCESS, e.getRejectionCause().toString());
        }
    }

    private ChargeCDRResponseDto createChargeCDRResultDto(List<WalletOperation> walletOperations, boolean returnWalletOperations) {

        ChargeCDRResponseDto result = new ChargeCDRResponseDto();
        BigDecimal amountWithTax = BigDecimal.ZERO;
        BigDecimal amountWithoutTax = BigDecimal.ZERO;
        BigDecimal amountTax = BigDecimal.ZERO;
        for (WalletOperation walletOperation : walletOperations) {
            if (returnWalletOperations) {
                WalletOperationDto walletOperationDto = new WalletOperationDto(walletOperation);
                result.getWalletOperations().add(walletOperationDto);
            }
            amountWithTax = amountWithTax.add(walletOperation.getAmountWithTax() != null ? walletOperation.getAmountWithTax() : BigDecimal.ZERO);
            amountWithoutTax = amountWithoutTax.add(walletOperation.getAmountWithoutTax() != null ? walletOperation.getAmountWithoutTax() : BigDecimal.ZERO);
            amountTax = amountTax.add(walletOperation.getAmountTax() != null ? walletOperation.getAmountTax() : BigDecimal.ZERO);
        }
        if (returnWalletOperations) {
            result.setWalletOperationCount(result.getWalletOperations().size());
        }
        result.setAmountTax(amountTax);
        result.setAmountWithoutTax(amountWithoutTax);
        result.setAmountWithTax(amountWithTax);
        return result;
    }

    /**
     * Allows the user to reserve a CDR, this will create a new reservation entity attached to a wallet operation. A reservation has expiration limit save in the provider entity
     * (PREPAID_RESRV_DELAY_MS)
     *
     * @param cdrLine String of CDR
     * @param ip where request came from
     * @return Available quantity and reservationID is returned. if the reservation succeed then returns -1, else returns the available quantity for this cdr
     * @throws MeveoApiException Meveo api exception
     * @throws BusinessException business exception.
     */
    public CdrReservationResponseDto reserveCdr(String cdrLine, String ip) throws MeveoApiException, BusinessException {
        System.out.println("cdrLine from reserve" + cdrLine);
        long masterInitial = new Date().getTime();
        CdrReservationResponseDto result = new CdrReservationResponseDto();
        // TODO: if insufficient balance retry with lower quantity
        result.setAvailableQuantity(-1);

        if (StringUtils.isBlank(cdrLine)) {
            missingParameters.add("cdr");
        }

        handleMissingParameters();

        CSVCDRParser cdrParser = cdrParsingService.getCDRParser(currentUser.getUserName(), ip);

        List<EDR> edrs;
        try {

            CDR cdr = cdrParser.parseCDR(cdrLine);

            edrs = cdrParsingService.getEDRList(cdr);
            for (EDR edr : edrs) {
//                BigDecimal initialQuantity = edr.getQuantity();
                edrService.create(edr);
                try {
                    long initial = new Date().getTime();
                    Reservation reservation = usageRatingService.reserveUsageWithinTransaction(edr, false);
                    log.info("[{}] reserveUsageWithTransaction done after {}", edr.getAccessCode(), new Date().getTime() - initial);
                    if (edr.getRatingRejectionReason() != null) {
                        throw new MeveoApiException(MeveoApiErrorCodeEnum.NO_MATCHING_CHARGE, edr.getRatingRejectionReason());
                    }
                    result.setReservationId(reservation.getId());
//                    if(!reservation.getQuantity().equals(initialQuantity)) {
//                        result.setAvailableQuantity(reservation.getQuantity().longValue());
//                    }
                } catch (InsufficientBalanceException e) {
                    log.trace("Failed to rate EDR {}: {}", edr, e.getRejectionReason());
                    throw new MeveoApiException(MeveoApiErrorCodeEnum.INSUFFICIENT_BALANCE, e.getMessage());

                } catch (RatingException e) {
                    log.trace("Failed to rate EDR {}: {}", edr, e.getRejectionReason());
                    throw new MeveoApiException(MeveoApiErrorCodeEnum.NO_MATCHING_CHARGE, e.getMessage());

                } catch (BusinessException e) {
                    log.error("Failed to rate EDR {}: {}", edr, e.getMessage(), e);
                    throw new MeveoApiException(MeveoApiErrorCodeEnum.BUSINESS_API_EXCEPTION, e.getMessage());
                }
                log.info("[{}] reserveCdr done after {}", edr.getAccessCode(), new Date().getTime() - masterInitial);

            }
        } catch (CDRParsingException e) {
            log.error("Error parsing cdr={}", e.getRejectionCause());
            throw new MeveoApiException(MeveoApiErrorCodeEnum.INVALID_ACCESS, e.getRejectionCause().toString());
        }
        return result;
    }

    /**
     * Confirms the reservation
     *
     * @param reservationDto Prepaid reservation's data
     * @param ip where request came from
     * @throws MeveoApiException Meveo api exception.
     */
    public void confirmReservation(PrepaidReservationDto reservationDto, String ip) throws MeveoApiException {
        log.info("[{}] confirmReservation {}", currentUser, reservationDto);
        long rezervationId = reservationDto.getReservationId();
        if (rezervationId > 0) {
            try {
                Reservation reservation = reservationService.findById(rezervationId); //some of them for some unknown reason take up to 5s (1 out of 600)
                if (reservation == null) {
                    throw new BusinessException("CANNOT_FIND_RESERVATION");
                }
                if (reservation.getStatus() != ReservationStatus.OPEN) {
                    EDR thisEdr = reservation.getOriginEdr();
                    DateTimeFormatter formatter =
                            DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'");
                    long totalQuantity = thisEdr.getQuantityLeftToRate().add(thisEdr.getQuantity()).longValue();
                    String cdrLine = OffsetDateTime.now(ZoneOffset.ofHours(2)).format(formatter) + ";" + String.valueOf(totalQuantity) + ";" + thisEdr.getAccessCode() + ";" + thisEdr.getParameter1() + ";" + thisEdr.getParameter2() + ";" + thisEdr.getParameter3()  + ";" + thisEdr.getParameter4();
                    Reservation newReservation = reservationService.findById(this.reserveCdr( cdrLine ,ip).getReservationId());
                    reservation = newReservation;
                    log.info("RESERVATION_NOT_OPEN");
                }

                if (reservationDto.getConsumedQuantity().compareTo(reservation.getQuantity()) == 0 || reservationDto.getConsumedQuantity().compareTo(reservation.getQuantity()) < 0) {

                    long time = new Date().getTime();
                    reservationService.cancelPrepaidReservation(reservation);
                    log.info("reservation and wo deleted after {} ms for rezervation = {}", new Date().getTime() - time, rezervationId); //average 46ms

                    EDR edr = reservation.getOriginEdr();  //some of them for some unknown reason take up to 5s (1 out of 900)
                    edr.setQuantity(reservationDto.getConsumedQuantity());

                    long timeEdr = new Date().getTime();
                    rateUsage(edr, new ChargeCDRDto());
                    log.info("usage rated after {} ms for rezervation = {}", new Date().getTime() - timeEdr, rezervationId); // average 12ms

                } else {
                    throw new BusinessException("CONSUMPTION_OVER_QUANTITY_RESERVED");
                }

            } catch (BusinessException e) {
                log.error("[{}] Failed to confirm reservation {} {}", currentUser.unProxy(), e.getMessage(), reservationDto);
                throw new MeveoApiException(e.getMessage());
            }
        } else {
            missingParameters.add("reservation");
            handleMissingParameters();
        }
    }

    private List<WalletOperation> rateUsage(EDR edr, ChargeCDRDto chargeCDRDto) throws MeveoApiException {

        List<WalletOperation> walletOperations = null;
        try {
            walletOperations = usageRatingService.rateUsageWithinTransaction(edr, chargeCDRDto.isVirtual(), chargeCDRDto.isRateTriggeredEdr(), chargeCDRDto.getMaxDepth(), 0);

        } catch (InsufficientBalanceException e) {
            log.trace("Failed to rate EDR {}: {}", edr, e.getRejectionReason());
            throw new MeveoApiException(MeveoApiErrorCodeEnum.INSUFFICIENT_BALANCE, e.getMessage());

        } catch (RatingException e) {
            log.trace("Failed to rate EDR {}: {}", edr, e.getRejectionReason());
            throw new MeveoApiException(MeveoApiErrorCodeEnum.NO_MATCHING_CHARGE, e.getMessage());

        } catch (BusinessException e) {
            log.error("Failed to rate EDR {}: {}", edr, e.getMessage(), e);
            throw new MeveoApiException(MeveoApiErrorCodeEnum.BUSINESS_API_EXCEPTION, e.getMessage());
        }

        return walletOperations;
    }

    /**
     * Cancels the reservation
     *
     * @param reservationDto Prepaid reservation's data
     * @param ip where request came from
     * @throws MeveoApiException Meveo api exception.
     */
    public void cancelReservation(PrepaidReservationDto reservationDto, String ip) throws MeveoApiException {
        if (reservationDto.getReservationId() > 0) {
            try {
                Reservation reservation = reservationService.findById(reservationDto.getReservationId());
                if (reservation == null) {
                    throw new BusinessException("CANNOT_FIND_RESERVATION");
                }
                if (reservation.getStatus() != ReservationStatus.OPEN) {
                    throw new BusinessException("RESERVATION_NOT_OPEN");
                }
                reservationService.cancelPrepaidReservation(reservation);
            } catch (BusinessException e) {
                log.error("Failed to cancel reservation ", e);
                throw new MeveoApiException(e.getMessage());
            }
        } else {
            missingParameters.add("reservation");
            handleMissingParameters();
        }
    }

    /**
     * Notify of rejected CDRs - for each of rejected lines, trigger a notification
     *
     * @param cdrList A list of rejected CDR lines (can be as json format string instead of csv line)
     */
    @Asynchronous
    public void notifyOfRejectedCdrs(CdrListDto cdrList) {

        for (String cdrLine : cdrList.getCdr()) {

            CDR cdr = new CDR();
            cdr.setLine(cdrLine);
            try {
                defaultObserver.cdrRejected(cdr);
            } catch (Exception e) {
                log.error("Failed to notify of rejected CDR {}", cdr, e);
            }
        }
    }

}
