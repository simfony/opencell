package org.meveo.api.rest.billing;

import org.meveo.api.dto.ActionStatus;
import org.meveo.api.rest.IBaseRs;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

@Path("/billing/rerate")
@Consumes({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
@Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
public interface ReRatingRs extends IBaseRs {

    /**
     * Execute rerate for non billed transactions with an updated price plan
     *
     * @return Request processing status
     */
    @GET
    ActionStatus executeReRate();
}
