package org.meveo.api.rest.billing.impl;

import org.apache.commons.lang3.tuple.Triple;
import org.meveo.api.dto.ActionStatus;
import org.meveo.api.dto.ActionStatusEnum;
import org.meveo.api.logging.WsRestApiInterceptor;
import org.meveo.api.rest.billing.ReRatingRs;
import org.meveo.api.rest.impl.BaseRs;
import org.meveo.service.billing.impl.ReRatingBasedOnPricePlanService;
import org.slf4j.Logger;

import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.interceptor.Interceptors;
import java.util.Arrays;
import java.util.List;

@RequestScoped
@Interceptors({WsRestApiInterceptor.class})
public class RaRatingRsImpl extends BaseRs implements ReRatingRs {

    @Inject
    private ReRatingBasedOnPricePlanService reRatingBasedOnPricePlanService;

    @Inject
    protected Logger log;

    @Override
    public ActionStatus executeReRate() {
        ActionStatus actionStatus = new ActionStatus();

        try {
            reRatingBasedOnPricePlanService.updateStatusToReRateForApplicableTransactions();

            Triple<Integer, Integer, List<String>> result = reRatingBasedOnPricePlanService.reRateApplicableTransactions();

            String errorMessages = Arrays.toString(result.getRight().toArray());
            actionStatus.setMessage(
                    String.format("NumberOfItemsToProcess = %d, NumberOfSuccessFulRecords = %d, ErrorMessages = %s"
                            , result.getLeft(), result.getMiddle(), errorMessages
                    )
            );
        } catch (Exception e) {
            log.error("Error executing rerating", e);
            actionStatus.setMessage(e.getMessage());
            actionStatus.setStatus(ActionStatusEnum.FAIL);
        }

        return actionStatus;
    }
}
